package testng.practise.multibrowser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class MultiBrowserTestNg {
	public WebDriver driver;

	@Parameters("browser")

	@BeforeClass

	// Passing Browser parameter from TestNG xml

	public void beforeTest(String browser) {

		// If the browser is Firefox, then do this

		if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "C://driver//chromedriver.exe");

			driver = new ChromeDriver();
		}

		// If browser is IE, then do this

		else if (browser.equalsIgnoreCase("ie"))

		{

			// Here I am setting up the path for my IEDriver

			System.setProperty("webdriver.ie.driver", "C://driver//IEDriverServer_x64_3.6.0//IEDriverServer.exe");

			driver = new InternetExplorerDriver();

		} else if (browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "C://driver//geckodriver.exe");
			driver = new FirefoxDriver();
		}
	}

	@Test

	public void navigate() {
		driver.get("https://www.t-mobile.com/");

	}

	@AfterClass
	public void afterTest() {

		//driver.quit();

	}
}
