package testng.practise.parallel;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
public class ParallelTestNg {
	public WebDriver driver;

	

	@BeforeMethod
	public void beforeMethod() {
		System.setProperty("webdriver.chrome.driver", "C://driver//chromedriver.exe");
		driver = new ChromeDriver();
		
		
		
	}
	
	@Test
    public void openAmazonNavigationPage() {
		driver.get("https://www.amazon.com/");
		//driver.quit();
		System.out.println("hello from ama");

    }
 
    @Test
    public void testMethodsTwo() {
    	driver.get("https://www.google.com");
    	System.out.println("hello from google");

    }
	@AfterMethod
	public void afterMethod() {
	//driver.close();
	}

}
